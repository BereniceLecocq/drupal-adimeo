# Drupal-Adimeo

## Name
Adimeo recrutement project.

## Description
Add related events block to event nodes.
Add QueueWorker to unpublish event with cron.

## Installation
```bash
# Clone project with SSH.
git clone git@gitlab.com:BereniceLecocq/drupal-adimeo.git
# Go to project folder.
cd drupal-adimeo
# Launch DDEV (For DDEV installation, see https://ddev.readthedocs.io/en/stable/users/install/ddev-installation/#linux).
## You can change project configuration for DDEV if you needed, here : .ddev/config.yaml
ddev start
# Install project dependencies.
ddev composer install
# Install default Drupal database.
ddev import-db --file=dumps/dump.sql
# Install files example for adimeo test.
sudo cp -r dumps/files/ web/sites/default/
# Install Drupal configuration.
ddev drush cim
# Clear cache of Drupal.
ddev drush cr
```
## Usage
```bash
# Go on https://adimeo-berenice.ddev.site/ for the website
## To connect with Admin account : 
ddev drush uli
```

See my working time in "dumps/Working Time.pdf" file.
