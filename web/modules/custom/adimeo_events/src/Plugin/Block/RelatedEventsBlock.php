<?php

declare(strict_types=1);

namespace Drupal\adimeo_events\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Drupal\adimeo_events\Service\EventsHelper;

/**
 * Provides a list of events related to the current event displayed.
 *
 * @Block(
 *   id = "adimeo_related_events",
 *   admin_label = @Translation("Adimeo Related Events"),
 *   category = @Translation("Adimeo"),
 * )
 */
class RelatedEventsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The Adimeo events service.
   *
   * @var \Drupal\adimeo_events\Service\EventsHelper
   */
  protected EventsHelper $EventsHelper;

  /**
   * Constructs a new RelatedEventsBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param RouteMatchInterface $route_match
   *   The current route match.
   * @param EventsService $eventsHelper
   *   The adimeo event helper.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RouteMatchInterface $route_match,
    EventsHelper $eventsHelper
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
    $this->eventsHelper = $eventsHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('adimeo_events.events_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];

    // Get current node.
    $currentNode = $this->routeMatch->getParameter('node');

    if ($currentNode instanceof NodeInterface) {
      // Get event type.
      $eventType = $currentNode->get('field_event_type')->target_id;
      // Get & build related events.
      $relatedEvents = $this->eventsHelper->getRelatedEvents($eventType);
      $build['content'] = $relatedEvents;
    }

    return $build;
  }

}
