<?php

namespace Drupal\adimeo_events\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Plugin implementation of the status_code_report queueworker.
 *
 * @QueueWorker (
 *   id = "adimeo_events_unpublish_events",
 *   title = @Translation("Unpublish past events"),
 *   cron = {"time" = 10}
 * )
 */
class UnpublishEvents extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($eventId) {
    if (empty($eventId)) {
      return;
    }
    $event = $this->entityTypeManager->getStorage('node')->load($eventId);
    if ($event instanceof NodeInterface) {
      $event->setUnpublished();
      $event->save();
    }
  }

}