<?php

declare(strict_types = 1);

namespace Drupal\adimeo_events\Service;

/**
 * Provides interface for Adimeo events helper.
 */
interface EventsHelperInterface {

  /**
   * Returns the related events, not related if under 3 results.
   *
   * @param string $eventType
   *   The event type of current node.
   *
   * @return array
   *   The list of related events markup.
   */
  public function getRelatedEvents(string $eventType): array;

  /**
   * Check if events must be unpublished.
   *
   * @return int
   *   The number of unpublished events.
   */
  public function checkToUnpublishPastEvents(): int;

}