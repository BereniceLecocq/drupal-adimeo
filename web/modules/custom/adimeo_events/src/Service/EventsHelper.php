<?php

declare(strict_types = 1);

namespace Drupal\adimeo_events\Service;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Queue\QueueFactory;

/**
 * Provides helper for Adimeo events.
 */
class EventsHelper implements EventsHelperInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queueFactory;

  /**
   * The current date.
   */
  protected string $dateNow;

  /**
   * EventsHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.   
   * @param StateInterface $state
   *   The state service.
   * @param TimeInterface $time
   *   The time service.
   * @param QueueFactory $queueFactory
   *   The queue service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    StateInterface $state,
    TimeInterface $time,
    QueueFactory $queueFactory
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->state = $state;
    $this->time = $time;
    $this->queueFactory = $queueFactory;
    // Set date's now.
    $this->dateNow = DrupalDateTime::createFromTimestamp($this->time->getCurrentTime())->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
  }

  /**
   * Get a list of events depend on event type of current node, completed if event type is NULL.
   *
   * @param string $eventType
   *   The event type of current node.
   * @param bool $afterNow
   *   The event end date is after now or not.
   *
   * @return array
   *   The list of matching events.
   */
  private function getEvents(string $eventType = NULL, bool $afterNow = TRUE): array {
    // Construct query base to get events opened after now.
    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->latestRevision()
      ->accessCheck(TRUE)
      ->condition('type', 'event')
      ->condition('status', 1)
      ->sort('field_date_range.value', 'ASC');

    // Get date comparition type depend on after or before now.
    $dateCompare = ($afterNow ? '>=' : '<=');
    $query->condition('field_date_range.end_value', $this->dateNow, $dateCompare);

    // If event type is implemented, we add it to query.
    if (!empty($eventType)) {
      $query->condition('field_event_type', $eventType);
    }

    // Return array of events.
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getRelatedEvents(string $eventType): array {
    // Get related events with the same event type.
    $events = $this->getEvents($eventType);
    $countEvents = count($events);

    // If haven't less 3 related events.
    if ($countEvents < 3) {
      // Get other events with other event type.
      $otherEvents = $this->getEvents();
      // Merge result with other event type.
      $events = array_merge($events, $otherEvents);
    }

    // Keep 3 events.
    $events = array_slice($events, 0, 3);

    // Return events markup with cache managed.
    $cache = new CacheableMetadata();
    $events = $this->entityTypeManager->getStorage('node')->loadMultiple($events);
    $cache->addCacheableDependency($events);
    $eventsBuilder = $this->entityTypeManager->getViewBuilder('node')->viewMultiple($events, 'teaser');
    $cache->applyTo($eventsBuilder);
    return $eventsBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public function checkToUnpublishPastEvents(): int {
    // Get last run timestamp.
    $cronLastRun = $this->state->get('adimeo_events.cron_last_run', 0);

    // Don't run the cron if the last run was less than 30 minutes ago.
    if ($this->time->getCurrentTime() < $cronLastRun + 1800) {
      return 0;
    }

    // Get events to unpublish them.
    $eventsToUnpublish = $this->getEvents($eventType = NULL, $afterNow = FALSE);

    // Unblish events with queue worker.
    $queue = $this->queueFactory->get('adimeo_events_unpublish_events');
    foreach($eventsToUnpublish as $eventId) {
      $queue->createItem($eventId);
    }

    // Set last run timestamp.
    $this->state->set('adimeo_events.cron_last_run', $this->time->getCurrentTime());

    // Return number of unpublished events.
    return count($eventsToUnpublish);
  }

}